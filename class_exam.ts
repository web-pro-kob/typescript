class Person {
    public constructor(private readonly name:string) {
    }
    public getName(): string {
      return this.name;
    }
  }
  
  const person = new Person("Jane");
  console.log(person);
  console.log(person.getName());
  
  interface Shape {
    getArea: () => number;
  }
  
  class Rectangle1 implements Shape {
    protected readonly width: number;
    protected readonly height: number;
  
    public constructor(width: number, height: number) {
      this.width = width;
      this.height = height;
    }
  
    public getArea(): number {
      return this.width * this.height;
    }
  }
  
  class Square extends Rectangle1 {
    public constructor(width: number) {
      super(width, width);
    }
  
    public toString(): string {
      return `Square [${this.width}]`;
    }
  }
  
  const sq = new Square(10);
  console.log(sq);
  console.log(sq.getArea());
  console.log(sq.toString());
  
  abstract class Polygon {
    public abstract getArea(): number;
  
    public toString(): string {
      return `Polygon[area=${this.getArea()}]`
    }
  }
  
  class Rectangle2 extends Polygon {
    public constructor(protected readonly width: number, protected readonly height: number) {
      super();
    }
    public getArea(): number {
      throw new Error("Method not implemented.");    
    }
  }
  
  const rect2:Rectangle2 = new Rectangle2(50, 10);
  console.log(rect2);
  console.log(rect2.toString());
  